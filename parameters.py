# -*- coding: utf-8 -*-
"""
Created on Wed Mar 11 09:30:34 2020

@author: arthu
"""
#group simulation(s) parameters
_input = 4
_g = 6
_replication = 10;
#random
_seed = 1998
_connectivity_seed = 1998
_boostrap_seed = 1998
#network simulation parameters
_simulation_time = 20      # simulation time of the network (in seconds)
_simulation_step = 0.01    # simulation time step (in ms)
_connectivity_ratio = 0.4  # ratio of connexion for a post-synaptic neuron vs total number of neuron; each neuron receives connectivity_ratio*Number_of_neuron input
_synaptic_delay = 1.5      # synaptic delay (in ms)
_Vm_jump_after_EPSP = 0.2  # increase of the posynaptic neuron membrane potential after EPSP (in mV)
_N_brunnel = 12500         # number of neuron in the non-downscaled brunnel network
_downscale_factor = 12.5   # downscaling factor of the network size
_fraction_excitatory = 0.8 # fraction of excitatory synapse in the network
#neuron parameters
_membrane_time_cst = 20.0  # membrane time constant (in ms)
_offset = 0.0              # offset to the membrane (in mV)
_Vreset = 10.0             # potential of reset after spiking added to offset (in mV)
_threshold = 20.0  	       # Spiking threshold added to offset (in mV)
_refractory_period = 2.0   # time constant of refractory period (in ms)
#connectivity model parameters
_initial_bam = 2           # initial size m0 for Barabasi Albert model
_probability_dbam = 0.33   # probability for a=b=c in directed Barabasi Albert model
_dimension_rgg = 3         # dimension of R^d space where neurons are placed in Geometric model
_radius_rgg = 0.5          # radius/distance under which a connection is made in Geometric model
_probability_ws = 0.2      # probability to rewire a connection Watts Strogatz model
_alpha_rhgg = 1.5          # inverse of dispersion on hyberbolic disk boundary in Hyperbolic Geometric model
_radius_rhgg = 5           # radius of hyperbolic disk in Hyperbolic Geometric model
_cluster_size_hn = 10      # cluster size in Hierarchical Network model
_probability_hn = 0.8      # probability of intracluster connection in Hierarchical Network model
_cluster_init_hmn = 125    # initial cluster size in Hierarchical Modular Network model
_alpha_hmn = 2             # related to probability in Hierarchical Modular Network model
#connectivity parameters
_connect_model = 'default' # connection model used for connecting neuron
                           # default, rand, semirand, erdosrenyi, bam, dbam,
                           # rgg, drgg, ws, rhgg, drhgg, hn, hmn, ddreg
_connect_models = ['default', 'rand', 'semirand', 'erdosrenyi', 'rgg', 'drgg', 'ws', 'rhgg', 'drhgg', 'hn', 'hmn']#['default', 'rand', 'semirand', 'erdosrenyi', 'bam', 'dbam', 'rgg', 'drgg', 'ws', 'rhgg', 'drhgg', 'hn', 'hmn', 'ddreg']
#measures parameters
_dynamics_key = 'dynamics' # dynamics label key used in the data to mark the dynamics in functional measures
_model_key = 'model'       # model label key used in the data to mark the model in connectivity measures
_top_alg_measures = ['dist_beta_0_max', 'dist_beta_0_halfmax_threshold', 'dist_beta_0_auc', 'dist_beta_1_max', 'dist_beta_1_max_threshold', 'dist_beta_1_auc', 'sync_beta_0_max', 'sync_beta_0_halfmax_threshold', 'sync_beta_0_auc', 'sync_beta_1_max', 'sync_beta_1_max_threshold', 'sync_beta_1_auc']
                           # ordered topological algebraic measures key
#resampling model parameters
_percentage_usimple = 0.367# percentage of added new edges                        
#resampling parameters
_count = 10                # number of resampled model produced by the algorithms
_resample_model = 'usimple'# resampling model to get access to the resampled data
                           # dsimple, usimple
#betti curves sampling parameters
_max_threshold = 1         # filtering max threshold
_measure_type = 'dist'     # similarity measurement type (dist, sync)
_measure_types = ['dist', 'sync']
_binning_step = 0.00001    # filtering binning step
_max_dimension = 1         # highest dimension computed

#rasterplot parameters
_t_start = 15              # time at which to start drawing in seconds
_t_end = 16                # time at which to stop drawing in seconds
_hist_bin_step = 1e-03     # step for the time binning used by the histogram 

# terminal parameters
_runRipser_name = 'runRipser.sh'