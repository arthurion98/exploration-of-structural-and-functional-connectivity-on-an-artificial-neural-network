# -*- coding: utf-8 -*-
"""
Created on Wed Jun  3 10:12:53 2020

@author: Tâm Nguyên
"""
import numpy as np;
import sklearn.decomposition as sk;
import utility;
import matplotlib.pyplot as plt;
import parameters;

"""compute the PCA of the measures
"""
#file parameters
connect_models = parameters._connect_models;
#measures parameters
dynamics_key = parameters._dynamics_key;
model_key = parameters._model_key;
top_alg_measures = parameters._top_alg_measures;


def DrawPCA():
    # import Dynamics Label and phase plane
    DL = utility.readDynamicsLabel('data/measures/dynamics_label.txt');
    
    # setup colors
    dynamics_labels = ['SR','SR_SI','SR_0','AI','Alt','Alt>SR_0','Alt<>SR_0'];
    
    colors = {};
    colors['SR'] = 'dodgerblue';
    colors['SR_SI'] = 'steelblue';
    colors['SR_0'] = 'lightskyblue';
    colors['AI'] = 'coral';
    colors['Alt'] = 'yellowgreen';
    colors['Alt>SR_0'] = 'aquamarine';
    colors['Alt<>SR_0'] = 'limegreen';
    
    colors['default'] = 'pink';
    colors['rand'] = 'maroon';
    colors['semirand'] = 'orangered';
    colors['erdosrenyi'] = 'sandybrown';
    colors['rgg'] = 'yellowgreen';
    colors['drgg'] = 'olivedrab';
    colors['rhgg'] = 'lime';
    colors['drhgg'] = 'aquamarine';
    colors['hn'] = 'lightskyblue';
    colors['hmn'] = 'dodgerblue';
    colors['ws'] = 'violet';
    
    # import data
    data_list = []; # list of data
    data_regime = []; # corresponding regime
    data_cm = []; # corresponding connect model
    
    for cm in connect_models:
        for cseedling in range(0,1): #replication
            for g in [2,4,6]:
                for inp in [1,2,3,4]:
                    for seedling in range(0, parameters._replication):
                        regime = DL[cm][int(g/2)-1][inp-1][seedling];
                        data = utility.readData('data/measures/functional_measures_{}_{}_i={}_g={}_s={}.csv'.format(cm, cseedling, inp, g, seedling), dynamics_key, model_key);
                        
                        data_list.append(data[0]); # extracted as a list of a single data element, need to get the first element
                        data_regime.append(regime);
                        data_cm.append(cm);
    
    # feature scale system matrix
    X = utility.dataToMatrix(data_list, top_alg_measures);
    fsX = utility.featureScaleMatrix(X);
    
    # create a dict of list of separated feature scaled trial according to their regime
    fsXsepR = {}
    for d in dynamics_labels: # init dict
        fsXsepR[d] = [];
    
    l_d = len(data_list);
    for i in range(0,l_d): # create separated list
        fsXsepR[data_regime[i]].append(fsX[i,:]);
        
    # create a dict of list of separated feature scaled trial according to their connect model
    fsXsepM = {}
    for cm in connect_models: # init dict
        fsXsepM[cm] = [];
    
    l_d = len(data_list);
    for i in range(0,l_d): # create separated list
        fsXsepM[data_cm[i]].append(fsX[i,:]);
    
    # PCA
    pca = sk.PCA(n_components=2);
    pca.fit(fsX);
    pca_score = pca.explained_variance_ratio_;
    V = pca.components_;
    
    print(pca_score);
    print(V);
    
    # draw dynamics labelled PCA
    plt.figure(figsize=(12.0, 12.0));
    
    for d in dynamics_labels: # draw each regime in a color
        x = [];
        y = [];
        l_sl = len(fsXsepR[d]);
        for k in range(0,l_sl):
            x.append(np.matmul(fsXsepR[d][k], V[0])); # z1 with first component
            y.append(np.matmul(fsXsepR[d][k], V[1])); # z2 with second component
            
        plt.scatter(x,y,color=colors[d], label=r'${}$'.format(d));
        
    plt.xlabel(r'$z_1$');
    plt.ylabel(r'$z_2$');
    plt.title(r'$PCA \ regime \ labelled$');
    plt.legend();
    plt.savefig('data/pca/regime_pca.png', format="png", dpi=300);
    plt.close();
    
    # draw model labelled PCA
    plt.figure(figsize=(12.0, 12.0));
    
    for cm in connect_models: # draw each model in a color
        x = [];
        y = [];
        l_sl = len(fsXsepM[cm]);
        for k in range(0,l_sl):
            x.append(np.matmul(fsXsepM[cm][k], V[0])); # z1 with first component
            y.append(np.matmul(fsXsepM[cm][k], V[1])); # z2 with second component
            
        plt.scatter(x,y, color=colors[cm], label=r'${}$'.format(cm));
        
    plt.xlabel(r'$z_1$');
    plt.ylabel(r'$z_2$');
    plt.title(r'$PCA \ model \ labelled$');
    plt.legend();
    plt.savefig('data/pca/model_pca.png', format="png", dpi=300);
    plt.close();
    
#try:
DrawPCA();
                        
#except Exception as e:
#    print('Error:')
#    print(str(e))
