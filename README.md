# Exploration of structural and functional connectivity on an artificial neural network

by Tâm NGUYEN

source code of the report used for the simulation, topological data analysis, measures computation and visualisation.

requires:
- python 3.6+
- Brian2 (https://briansimulator.org/)
- PySpike (http://mariomulansky.github.io/PySpike/)
- Scikit-learn (https://scikit-learn.org/stable/)
- Ripser (https://github.com/Ripser/ripser)
- BCTPy (https://pypi.org/project/bctpy/)

the original code for the project was provided by Jean-Baptiste BARDIN, Gard SPREEMANN and Kathryn HESS,
in their paper 'Topological exploration of artificial neuronal network dynamics'.
repository: https://github.com/JBBardin/Brunel_AlgebraicTopology

INSTALLATION NOTE:
as I had trouble setting up PySpike using pip, I installed it manually, and thus at the top of
similarity_measure.py, the sys environment variable is modified to reference the installation
folder of PySpike, this obviously won't work with other setup and you should modify the reference
accordingly.

to run the Ripser computations, we've compiled an automatically generated command file runRipser.sh
you will need to run it in a terminal while in the repository folder, assuming the Ripser installation
is in a folder outside that of the repository. To reference Ripser differently you can modify the runRipser.sh
generating script in ripser_link.py.

## Parameters

the various parameters used during the simulation are filed in parameters.py.

## Generating the Structural Connectivity

you should run the script generate_model.py once for the entire project, it outputs
several text files saving the structural connectivity of the various graph model to
data/connectivity.

```bash
python generate_model.py
```

## Simulation

~6h

you will need to run this step once for every point of the phase plane, indicating the
parameters input and g in the parameters.py file. simulation.py simulate the structural
connectivity and save the spike trains to data/simulation.

```bash
python simulation.py
```

## Computing the Functional Connectivity

~8h

once the simulation is run, the script similarity_measure.py produces the functional
connectivity under data/similarity in text files with a .lower_distance_matrix extension.

```bash
python similarity_measure.py
```

## Ripser and Persistent Homology

~1h

we use Ripser to perform persistent homology. for quality of life we provide commands file
that can run automatically the whole computation for a point of the phase plane runRipser.sh.

NOTE: Ripser can run out of memory, in which case it outputs a seg_fault

```bash
python ripser_link.py
```

then you can run the commands file in the terminal of your linking while in the repository folder,
at the same level as the scripts (and commands file). If you are using a Linux installation of Ripser
we recommend you perform the additional step:

```bash
dos2unix runRipser.sh
```

in order to get rid of any encoding error that may appear.

you can simply then have Ripser perform the computation (assuming it is installed in a folder
outside the repository, else you will need to modify ripser_link.py). output is stored in
data/ph.

```bash
./runRipser.sh
```

## Measures

~2d (18d if all the set of features is computed)

we have commented out certain section of the measure_computation.py since we did not perform
all measures in one go, you should get rid of the comments in order to either compute the
connectivity measures or all functional measures.

this step calculates the measures for all points of the phase plane and therefore assume you have
simulated, computed the functional connectivity and the persistent homology.

the visually identified labels for the regime are stored in data/measures/dynamics_label.txt
with the following format:

```bash
<name-of-graph>
<label-for-seed-0-input-1-g-2>,<label-for-seed-1-input-1-g-2>,...
<label-for-seed-0-input-2-g-2>,<label-for-seed-1-input-2-g-2>,...
...
-
<label-for-seed-0-input-1-g-4>,<label-for-seed-1-input-1-g-4>,...
<label-for-seed-0-input-2-g-4>,<label-for-seed-1-input-2-g-4>,...
...
-
<label-for-seed-0-input-1-g-6>,<label-for-seed-1-input-1-g-6>,...
<label-for-seed-0-input-2-g-6>,<label-for-seed-1-input-2-g-6>,...
...
...
<next-name-of-graph>
...
```

you can perform the measures using the measure_computation.py script which outputs to data/measures.

```bash
python measure_computation.py
```

## PCA

<1 min

once all measures have been done, we have setup the PCA to run only on the algebraic topological features
for now. it will output to labelling, one for the regime described in dynamics_label.txt and one for the
structural connectivity model used to data/pca.

```bash
python pca.py
```

## Visualisation

we provide the visualisation scripts used to generate the figure in the report.

Raster plot : (on a point of the phase plane basis; after simulation)

~1h

to data/rasterplot

```bash
python rasterplot.py
```

Betti curves : (on a point of the phase plane basis; after Ripser)

~2h

to data/betti_curves

```bash
python ph_view.py
```

Phase plane : (all phase plane done; after dynamics_label.txt)

<1 min

to data/phase_plane

```bash
python phaseplane.py
```

PCA : (all measures done; after Measures)

<1 min

to data/pca

```bash
python pca.py
```

## Use of the code

you may use any part of my original code however you deem fit, as long as you credit
me and link to this repository.

regarding the code which is based on the article of Jean-Baptiste BARDIN, Gard SPREEMANN
and Kathryn HESS, you will need to ask them for permission. here are the files which are
based on their work:

- simulation.py
- similarity_measure.py