# -*- coding: utf-8 -*-
"""
Created on Fri Feb 28 12:08:38 2020

@author: arthu
"""

import numpy as np;
import random;
import os;
import csv;

def saveConnectivity(N, pre_i, post_j, name):
    """create a connectivity matrix under a text file following the convention:
        '1' connected, '0' not connected, line by line with a '\n',
        please note that the connection are directional, lines correspond to
        presynaptic neurons while columns correspond to postsynaptic neurons
        :param N: number of neuron, index range from 0 to N-1
        :param pre_i: presynaptic neuron (index i) connecting to the postsynaptic neuron (index j) with same indexation
        :param post_j: postsynaptic neuron (index j) receiving connection from presynaptic neuron (index i) with same indexation
        :param name: the name and path to file to which the matrix is saved
        :return: null
    """
    #generating connectivity matrix
    connectivity_matrix = np.zeros((N,N),dtype=bool);
    for i,j in zip(pre_i, post_j):
        connectivity_matrix[i,j] = True;
    
    #creating output
    output = open(name,'w');
    for line in connectivity_matrix:
        for connected in line:
            buffer = '1,' if connected else '0,';
            output.write(buffer);
        output.write('\n');
    output.close();
    
    with open(name, 'rb+') as output: #remove ',\n'
        output.seek(-3, os.SEEK_END)
        output.truncate();
        output.close();

def saveRawConnectivity(pre_i, post_j, name):
    """directly save pre_i and post_j under the format
        'pre_i\n(list of values with ' ' in between)\npost_j\n(list of values with ' ' in between)\n
        :param pre_i: presynaptic neuron (index i) connecting to the postsynaptic neuron (index j) with same indexation
        :param post_j: postsynaptic neuron (index j) receiving connection from presynaptic neuron (index i) with same indexation
        :param name: the name and path to file to which the data is saved
        :return: null
    """
    #creating output
    output = open(name,'w');
    output.write('pre_i\n');
    for index_i in pre_i:
        output.write(str(index_i));
        output.write(' ');
    output.write('\n');
    output.write('post_j\n');
    for index_j in post_j:
        output.write(str(index_j));
        output.write(' ');
    output.write('\n');
    output.close();
    
def extractMatrixFromConnectivity(name, N):
    """extract pre_i, post_j from the saved raw connectivity, and build the
        adjacency matrix from it : row corresponds to i and column to j.
        the matrix takes values in {0, 1}, the diagonal is filled with 0.
        :param name: the name and path to file to which the data is saved
        :param N: Number of neurons
        :return: NxN adjacency matrix (pre_i row, post_j column)
    """
    #extract pre_i, post_j
    pre_i, post_j = extractRawConnectivity(name);
    
    #generating adjacency matrix
    adjacency_matrix = np.zeros((N,N));
    for i,j in zip(pre_i, post_j):
        adjacency_matrix[i,j] = 1;
        
    return adjacency_matrix;

def extractMatrixFromSimulationLowerMatrix(name, N):
    """extract the lower matrix from the saved lower matrix and build the
        adjacency matrix through use of symetry and filling the diagonal
        with 0, takes values in [0, 1] : row and columns to the neuron
        indexation used by pre_i and post_j
        :param name: the name and path to file to which the data is saved
        :param N: Number of neurons
        :return: NxN adjacency symetric matrix
    """
    #generating adjacency matrix
    adjacency_matrix = np.zeros((N,N));
    
    file = open(name,'r');
    lines = file.readlines();
    i = 1; # 1 to N-1
    for line in lines:
        data = line.split(',');
    
        j = 0; # 0 to i-1
        for d in data:
            if(d != '\n'): #last ',' separate a '\n'
                adjacency_matrix[i,j] = float(d);
                adjacency_matrix[j,i] = float(d); #symetry
                j += 1;
        i+= 1;
    
    return adjacency_matrix;
    
def extractRawConnectivity(name):
    """directly extract pre_i, post_j from the saved raw connectivity.
        :param name: the name and path to file to which the data is saved
        :return: pre_i the list of pre-synaptic neuron index to which post_j the post-synaptic neuron is connected
    """
    index = [];
    dim = -1; #0: i 1: j
    
    file = open(name,'r');
    lines = file.readlines();
    for line in lines:
        if(line[0] == 'p'): #separation between pre_i and post_j line start with a 'p'
            dim = dim + 1;
            index.append([]); #initialize new dimension array
        else: #data line start with a ' '
            data = line.split(' ');
            for d in data:
                if(d != '\n'): #last ' ' separate a '\n'
                    index[dim].append(int(d));
    
    return index[0],index[1];

def saveData(dictionary, name):
    """save a dictionary under a .csv file, first line are the keys
        second is their values.
        :param dictionary: dictionary to save
        :param name: path and name of the .csv file (must end in .csv)
        :return: null
    """
    #creating output
    output = open(name,'w');
    
    keys_string = '';
    values_string = '';
    for key in dictionary.keys():
        keys_string = '{}{},'.format(keys_string, key);
        values_string = '{}{},'.format(values_string, dictionary[key]);
    
    #get rid of the final comma ','
    keys_string = keys_string[0:-1];
    values_string = values_string[0:-1];
    
    #write output
    output.write(keys_string);
    output.write('\n');
    output.write(values_string);
    output.close();

def readData(name, dynamics_key, model_key):
    """extract data from a .csv file into the following format
        data = [{measure : value, ...},...]
        :param name: path and name of the .csv file (must end in .csv)
        :param dynamics_key: key used for the dynamics label
        :param model_key: key used for the model label
        :return: data in the format above
    """
    #extracting data
    data = []
    with open(name, newline='', encoding="utf-8-sig") as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            #save each line as a dictionary
            measures = dict(row);
            
            #convert to float the value, except label_key
            for measure,value in measures.items():
                if measure != dynamics_key and measure != model_key:
                    measures[measure] = float(value);
            
            data.append(measures);
            
    return data;

def readDynamicsLabel(name):
    """extract the dynamics label from a .txt file into the following format
        labels = {connect model : [[(g = 2, label seed 0),...], ...], ...}
        :param name: path and name of the .txt file (must end in .txt)
        :return: labels in the format above
    """
    labels = {};

    file = open(name,'r');
    lines = file.readlines();
    
    length = len(lines)
    for i in range(0,length):
        line = lines[i];
        if (i % 15) == 0 and i != length-1: #connectivity model
            connect_model = line[0:-1]; # remove \n
            
            sub_labels = [[[] for k in [1,2,3,4]] for m in [2,4,6]]; #first index g, second index input
            
            j = 0;
            counter = 3;
            
            inp = 0;
            g = 0;
        else:
            if (j % 5) == 0: # start of a replication series
                counter -= 1;
                inp = 1;
                g += 2;
            
            if (j % 5) != 4:
                regimes = line.split(',');
                regimes[len(regimes)-1] = regimes[len(regimes)-1][0:-1]; #remove \n
                
                sub_labels[int(g/2)-1][inp-1] = regimes; # conversion of g and inp to indexes
                inp += 1;
                
            if (j % 5) == 3 and counter < 1: # reach the end of the phase plane
                # save lables for the phase plane
                labels[connect_model] = sub_labels;
            j += 1;
    
    return labels;

def dataToMatrix(data_list, keys):
    """convert a list of data in the form of a dict of {measures : value}
        to a matrix of the format (index_data, index_measures), using keys for
        ordering the measures.
        :param data_list: the list of data to convert
        :param keys: ordered list of the measures keys to extract from data
        :return: matrix of the data and measures
    """
    l_d = len(data_list);
    l_k = len(keys);
    mat = np.zeros((l_d, l_k));
    
    for i in range(0,l_d):
        for j in range(0, l_k):
            mat[i,j] = data_list[i][keys[j]]; # extract value
    
    return mat;

def featureScaleMatrix(mat):
    """feature scale the matrix (data, measures) according to the measures.
        :param mat: the matrix to feature scale
        :return: feature scaled matrix
    """
    copymat = mat.copy(); # ensure no modification is made on original matrix
    
    l_ = copymat.shape;
    l_d = l_[0]; # first dimension
    l_k = l_[1]; # secund dimension
    
    fsmat = np.zeros(l_);
    
    for j in range(0,l_k):
        m_vector = copymat[:,j];
        
        # compute and save mean and std_dev
        mean = np.mean(m_vector);
        std_dev = np.std(m_vector);
        
        # feature scale
        for i in range(0,l_d):
            if std_dev != 0: #if std dev is 0 then the scaling just let everything be 0.
                fsmat[i,j] = float(m_vector[i]-mean)/std_dev;
                
    return fsmat;
        

def downsampleConnectivity(name, count, seed):
    """directly extract pre_i, post_j from the saved raw connectivity and resample
        from the set the edges not allowing edges to come more than once.
        the networks ensuing is smaller in term of edge count.
        :param name: the name and path to file to which the data is saved.
        :param count: the number of resampled networks produced.
        :param seed: seed for random number generator
        :return: null
    """
    random.seed(seed)
    
    conn_i, conn_j = extractRawConnectivity(name);
    length = len(conn_i); # assumed to be the same as conn_j
    
    for b in range(0,count):
        new_name = name[0:-4] + '_dsimple{}_{}.txt'.format(seed, b);
        new_conn_i = [];
        new_conn_j = [];
        #filter for double edges
        filtered_list = set(random.choices(list(zip(conn_i, conn_j)), k = length));
        for pair in filtered_list:
            new_conn_i += [pair[0]];
            new_conn_j += [pair[1]];
        
        #save newly sampled network
        saveRawConnectivity(new_conn_i, new_conn_j, new_name);
        
def upsampleConnectivity(name, N, count, p, seed):
    """directly extract pre_i, post_j from the saved raw connectivity and add
        up to p% random new edges.
        the networks ensuing is bigger in term of edge count.
        :param name: the name and path to file to which the data is saved.
        :param N: Number of neurons in original network
        :param count: the number of resampled networks produced.
        :param p: percent of new edges based on existing edges
        :param seed: seed for random number generator
        :return: null
    """
    random.seed(seed);
    
    conn_i, conn_j = extractRawConnectivity(name);
    length = len(conn_i); # assumed to be the same as conn_j
    
    K = int(length*p);
    
    for b in range(0,count): 
        new_name = name[0:-4] + '_usimple{}_{}.txt'.format(seed, b);
        filtered_list = set(zip(conn_i, conn_j));
        #add new connections
        for k in range(0,K):
            indexes = random.sample(list(range(0,N)),2);
            filtered_list.add((indexes[0], indexes[1]));
            
        new_conn_i = [];
        new_conn_j = [];
        #filter for double edges
        for pair in filtered_list:
            new_conn_i += [pair[0]];
            new_conn_j += [pair[1]];
            
        #save newly sampled network
        saveRawConnectivity(new_conn_i, new_conn_j, new_name);
            
def DEBUG_resampling_analyzer(name, resample_model, count, seed):
    """Analyze differences between original network and resampled network.
        :param name: the name and path to file to which the data is saved.
        :param resample_type: model used for resampling
        :param count: the number of resampled networks produced.
        :param seed: seed for random number generator used by resample algorithm
        :return: average resampled_connection/original_connection, maximum of resampled_connection/original_connection
    """
    conn_i, conn_j = extractRawConnectivity(name);
    length = len(conn_i); # assumed to be the same as conn_j
    
    average = 0;
    maximum = 0;
    for b in range(0, count):
        new_name = name[0:-4] + '_{}{}_{}.txt'.format(resample_model, seed, b);
        new_conn_i, new_conn_j = extractRawConnectivity(new_name);
        
        new_length = len(new_conn_i); # assumed to be the same as new_conn_j
        average += (new_length/length);
        maximum = max(1-(new_length/length), maximum);
    
    average = (average/count);
    return average, maximum;

def convolutionCrossCorrelationDistanceMatrix(sp, T, dt, trefract):
    """Calculate the distance matrix between spiketrains using a convolution
        with a Gaussian (sigma = trefract/2) and then a cross-correlation,
        the distance is then the maximum of the relation between the two spiketrains.
        Two Normalisation occurs, one regarding spike-gaussian convolution, and
        a second after the cross-correlation, both used the perfect tonic
        spike train as reference (time between spike is exactly trefract)
        :param sp: list of spiketrains (spiketrains are list of time where the spike occurs)
        :param T: time during which the simulation was run (seconds)
        :param dt: timestep for sampling of sp (seconds), should be a factor of T and trefract
        :param trefract: refractory time between spikes (seconds), should be a factor of T
        :return: distance matrix (symetrical)
    """
    N = int(T/dt);
    n = int(T/trefract);
    p = int(trefract/dt);
    
    #calculation of normalisation factors
    
    # generate perfect tonic spike train
    perfect_tonic_st = np.zeros(N+1);
    for i in range(0, n+1):
        perfect_tonic_st[i*p] = 1;
    
    # convolution with gaussian
    gaussian_samples = gaussian(int(trefract/2));
    convolved_perfect_tonic_st = np.convolve(perfect_tonic_st, gaussian_samples);
    
    gaussian_normalisation_factor = max(convolved_perfect_tonic_st);
    
    # autocorrelation
    autocorrelated_convolved_perfect_tonic_st = np.correlate(convolved_perfect_tonic_st, convolved_perfect_tonic_st);
    
    crosscorrelation_normalisation_factor = max(autocorrelated_convolved_perfect_tonic_st);
    
    #cross-correlation matrix
    n_neuron = sp.length;
    crosscorrelation_matrix = np.zeros((n_neuron, n_neuron));
    
    for i in range(0, n_neuron):
        for j in range(0, n_neuron):
            # convert sp to binary discrete signal
            sp_i = sp[i];
            sp_j = sp[j];
            
            st_i = np.zeros(N+1)
            for spike_t in sp_i:
                st_i[int(spike_t/dt)] = 1;
                
            st_j = np.zeros(N+1)
            for spike_t in sp_j:
                st_j[int(spike_t/dt)] = 1;
            
            # convolution with gaussian & normalisation I
            g_st_i = np.convolve(st_i, gaussian_samples)/gaussian_normalisation_factor;
            g_st_j = np.convolve(st_j, gaussian_samples)/gaussian_normalisation_factor;
            
            # cross correlation & normalisation II
            d_i_j = max(np.correlate(g_st_i, g_st_j))/crosscorrelation_normalisation_factor;
            
            # save distance to the matrix
            crosscorrelation_matrix[i,j] = d_i_j;
            
    return crosscorrelation_matrix;

def gaussian(sigma):
    """Generate a gaussian centered in 0, composed of 6*sigma + 1 samples,
        ranging from -3*sigma to 3*sigma with the 0 sample
        :param sigma: the standard deviation of the gaussian (integer)
    """
    x = np.arange(-3*sigma, 3*sigma, 1);
    gaussian_samples = np.exp(-((float(x)/sigma)**2)/2);
    
    return gaussian_samples;

#DEPRECATED
#not useful to compare different simulation
def featureScaleLowerMatrix(matrix):
    """feature scale (unit-based normalisation [0,1]) each value of the full matrix based on the lower matrix values
        :param matrix: the matrix to normalise
        :return: feature scaled matrix
    """
    lower_matrix_values = [];
    
    N = len(matrix);
    for i in range(1,N):
        for j in range(0,i):
            lower_matrix_values.append(matrix[i,j]);
            
    std = np.std(lower_matrix_values);

    matrix = matrix/std;
    
    return matrix;
        
def saveMeasureFullMatrix(matrix, name):
    """create a full-distance matrix under a text file following the convention:
        ',' between values, line by line with a '\n'
        :param matrix: the matrix to convert to text format
        :param name: the name and path to file to which the matrix is saved
        :return: null
    """
    #creating output
    output = open(name,'w');
    for line in matrix:
        for e in line:
            output.write('{},'.format(e));
        output.write('\n');
    output.close();
    
    #print('{},{}'.format(l,c));
    
    with open(name, 'rb+') as output: #remove ',\n'
        output.seek(-3, os.SEEK_END)
        output.truncate();
        output.close();
        
def saveMeasureLowerMatrix(matrix, name):
    """create a lower-distance matrix under a text file following the convention:
        ',' between values, line by line with a '\n', create considerably smaller
        file than full-distance, expect a symetric matrix.
        :param matrix: the matrix to convert to text format
        :param name: the name and path to file to which the matrix is saved
        :return: null
    """
    output = open(name,'w');
    
    N = len(matrix);
    for i in range(1,N):
        for j in range(0,i):
            output.write('{}'.format(matrix[i,j]));
            if (j < N-2): #last character
                output.write(',');
        if (j < N-2): #last character
            output.write('\n');
    output.close();
    
def sampledBettiCurves(name, dt, max_threshold):
    """sample from a ripser output file the betti curves, return the betti curve
        values indexed on their respective threshold multiple
        :param name: name of the ripser output file
        :param dt: threshold step used for sampling (start at threshold = 0)
        :param max_threshold: max threshold value used for ripser
        :return: list of the sampled values at each threshold step multiple for each dimension.
    """
    start = [];
    end = [];
    dim = -1; #allow to start at dimension 0
    
    file = open(name,'r');
    lines = file.readlines();
    for line in lines:
        if(line[0] == 'p'): #persistence dimension line start with a 'p'
            dim = dim + 1;
            start.append([]); #initialize new dimension array
            end.append([]);
        if(line[0] == ' '): #data line start with a ' '
            data = line.split(',');
            start_d = data[0];
            start_d = float(start_d[2:]); #get the start of the persistent element of dimension dim by removing the starting '['
            
            end_d = data[1];
            end_d = end_d[0:-2]; #get the end of the persistent element of dimension dim by removing the ending ')'
            if end_d[0] == ' ': #replace ' ' (blank) by the max_threshold + 1 ( +1 is just to get over the max_threshold) evaluated since it represents infinity
                end_d = max_threshold + 1;
            end_d = float(end_d);
            
            start[dim].append(start_d);
            end[dim].append(end_d);
    
    nmax = int(max_threshold/dt); #max number of multiples of dt
    betti_curves = np.zeros(((dim+1),nmax+1))
    
    for d in range(0, dim+1):
        size = len(start[d]); #taille de start et end
        for i in range(0, nmax+1):
            t = dt*i;
            
            for k in range(0, size): #increment the betti curve for each element present during ]t,t+dt]
                if(start[d][k] <= t+dt and end[d][k] > t):
                    betti_curves[d,i] = betti_curves[d,i] + 1;
    return betti_curves;

def defaultConnect(N, NE, C, Ce, seed):
    """randomly connect post-synaptic neurons to Ce excitatory neuron and C-Ce inhibitory neuron
        without self-connection.
        Original defaultConnect implementation of connections come from ExcInhNet_Ostojic2014_Brunel2000_brian2.py 
  	    written by Aditya Gilra to connect the synapses.
        modified by Tâm Nguyên to prevent self-connection
        :param N: Number of neurons
        :param NE: Number of excitatory neurons
        :param C: Number of connection received by post-synaptic neurons
        :param Ce: Number of excitatory connection received by post-synaptic neurons
        :param seed: seed for random number generator
        :return: conn_i the list of pre-synaptic neuron index to which conn_j the post-synaptic neuron is connected
    """
    random.seed(seed); # set seed for reproducibility of simulations
    
    conn_i = [];
    conn_j = [];
    for j in range(0,N):
		# sample Ce number of neuron indices out of NE neurons without the index j
        preIdxsE = random.sample(list(filter(lambda x : x != j, range(0, NE))), Ce);
    	# sample Ci=C-excC number of neuron indices out of inhibitory neurons without the index j
        preIdxsI = random.sample(list(filter(lambda x : x != j, range(NE, N))), C-Ce);
        conn_i += preIdxsE;
        conn_j += [j]*Ce;
        conn_i += preIdxsI;
        conn_j += [j]*(C-Ce);
    return conn_i, conn_j;

def randomConnect(N, seed):
    """randomly choose the number of connection a given neuron receive (from 0 to N-1),
        then choosee at random from which neurons it receives them without self-connection.
        :param N: Number of neurons
        :param seed: seed for random number generator
        :return: conn_i the list of pre-synaptic neuron index to which conn_j the post-synaptic neuron is connected
    """
    random.seed(seed); # set seed for reproducibility of simulations
    
    Cn = random.sample(range(0,N),N);
    conn_i = [];
    conn_j = [];
    for j in range(0,N):
		# sample Cn[j] number of neuron indices out of N neurons without the index j
        preIdxs = random.sample(list(filter(lambda x : x != j, range(0, N))), Cn[j])
        conn_i += preIdxs;
        conn_j += [j]*Cn[j];
    return conn_i, conn_j;

def semirandomConnect(N, C, seed):
    """randomly choose C neurons that will connect to a receiving post-synaptic neuron.
        :param N: Number of neurons
        :param C: Number of connection received by post-synaptic neurons
        :param seed: seed for random number generator
        :return: conn_i the list of pre-synaptic neuron index to which conn_j the post-synaptic neuron is connected
    """
    random.seed(seed); # set seed for reproducibility of simulations
    
    conn_i = [];
    conn_j = [];
    for j in range(0,N):
		# sample Cn[j] number of neuron indices out of N neurons without the index j
        preIdxs = random.sample(list(filter(lambda x : x != j, range(0, N))), C);
        conn_i += preIdxs;
        conn_j += [j]*C;
    return conn_i, conn_j;

def erdosRenyiConnect(N, C, seed):
    """evaluate independently each possible connection with probability p of being included.
        :param N: Number of neurons
        :param C: Expected number of connection received by post-synaptic neurons (p = C/(N-1))
        :param seed: seed for random number generator
        :return: conn_i the list of pre-synaptic neuron index to which conn_j the post-synaptic neuron is connected
    """
    random.seed(seed); # set seed for reproducibility of simulations
    
    p = C/(N-1);
    conn_i = [];
    conn_j = [];
    for j in range(0,N):    
        for i in range(0,N):
            if i != j:
                if random.random() < p:
                    conn_i += [i];
                    conn_j += [j];
    return conn_i, conn_j;

def barabasiAlbertConnect(N, m0, seed):
    """create a scale-free directed in-focused network which favour neuron wich receive a higher number of
        connection to receive more connection by an iteration process.
        :param N: Number of neurons
        :param m0: Size of initial fully connected network, and max additional connection given per new neuron 
        :param seed: seed for random number generator
        :return: conn_i the list of pre-synaptic neuron index to which conn_j the post-synaptic neuron is connected, total number of received input
    """
    random.seed(seed); # set seed for reproducibility of simulations
    
    current_nodes = [];
    buffer_nodes = random.sample(range(0,N),N);
    
    conn_i = [];
    conn_j = [];
    
    #create fully connected network of m0 nodes
    for i in range(0,m0):
        current_nodes += [buffer_nodes.pop()];
    for j in current_nodes:
        for i in current_nodes:
            if i != j:
                conn_i += [i];
                conn_i += [j];
                conn_j += [j];
                conn_j += [i];
                
    received = [m0-1]*m0;
    
    #create new nodes
    for m in range(m0,N):
        #try to connect m0 times the new_node to the current_nodes
        new_node = buffer_nodes.pop();
        new_connections = [];
        
        for k in range(0,m0):
            #create cumulative probability array
            number_current_nodes = len(current_nodes);
            cumulative_probability = [0]*number_current_nodes;
            previous_cumulative_probability = 0;
            for k in range(0,len(current_nodes)):
                if current_nodes[k] not in new_connections:    
                    cumulative_probability[k] = previous_cumulative_probability + received[k]/(np.sum(received));
                    previous_cumulative_probability = cumulative_probability[-1];
                else:
                    cumulative_probability[k] = previous_cumulative_probability;
                    #previous_cumulative_probability left untouched
            
            rand = random.random()*previous_cumulative_probability; #scaled due to computer imprecision and different connection
            for i in range(0,number_current_nodes):
                if rand < cumulative_probability[i]:
                    target_node = current_nodes[i];
                    if target_node not in new_connections:
                        new_connections += [target_node];
                        received[i] += 1;
                        break; #found the random node following the degree distribution
        
        conn_i += [new_node]*len(new_connections);
        conn_j.extend(new_connections);
        
    return conn_i, conn_j, np.sum(received);

def directedBarabasiAlbertConnect(N, a, b, c, din, dout, seed):
    """create a scale-free directed network which favour neuron wich receive a higher number of
        connection to receive more connection by an iteration process, while providing balanced in and out connection.
        (a + b + c = 1)
        :param N: Number of neurons
        :param a: Probability to add a new pre-synaptic neuron and its associated edge
        :param b: Probability to add a new edge
        :param c: Probability to add a new post-synaptic neuron and its associated edge
        :param din: tonic inputs received by all neurons
        :param dout: tonic outputs sent by all neurons
        :param seed: seed for random number generator
        :return: conn_i the list of pre-synaptic neuron index to which conn_j the post-synaptic neuron is connected, total number of received input
    """
    random.seed(seed);
    
    buffer_nodes = random.sample(range(0,N),N);
    #start with two nodes fully connected to prevent division by 0
    current_nodes = [buffer_nodes.pop()] + [buffer_nodes.pop()];
    c_in = [din+1]*len(current_nodes);
    c_out = [dout+1]*len(current_nodes);
    
    conn_i = [current_nodes[0]] + [current_nodes[1]];
    conn_j = [current_nodes[1]] + [current_nodes[0]];
    
    while len(buffer_nodes) > 0:
        if random.random() < a:
            if len(buffer_nodes) < 1:
                break;
            new_node = buffer_nodes.pop();
            
            #create cumulative probability array for c_in
            number_current_nodes = len(current_nodes);
            cumulative_probability = [0]*number_current_nodes;
            previous_cumulative_probability = 0;
            for k in range(0,len(current_nodes)):
                cumulative_probability[k] = previous_cumulative_probability + c_in[k]/(np.sum(c_in));
                previous_cumulative_probability = cumulative_probability[-1];
                
            #select old and connect new -> old
            rand = random.random()*previous_cumulative_probability; #scaled due to computer imprecision
            for i in range(0,number_current_nodes):
                if rand < cumulative_probability[i]:
                    target_node = current_nodes[i];
                    #connect new -> old
                    c_in[i] += 1; #old node is receiving the connection
                    c_out += [1+dout]; #new node is giving the connection
                    conn_i += [new_node];
                    conn_j += [target_node];
                    break; #found the old node following the degree distribution
            
        if random.random() < b:            
            #create cumulative probability array for c_out
            number_current_nodes = len(current_nodes);
            cumulative_probability = [0]*number_current_nodes;
            previous_cumulative_probability = 0;
            for k in range(0,len(current_nodes)):
                cumulative_probability[k] = previous_cumulative_probability + c_out[k]/(np.sum(c_out));
                previous_cumulative_probability = cumulative_probability[-1];
            
            #select pre
            index_pre = 0;
            rand = random.random()*previous_cumulative_probability; #scaled due to computer imprecision
            for i in range(0,number_current_nodes):
                if rand < cumulative_probability[i]:
                    pre_node = current_nodes[i];
                    index_pre = i;
                    c_out[i] += 1; #pre node is giving the connection
                    conn_i += [pre_node];
                    break; #found the pre node following the degree distribution
                    
            #create cumulative probability array for c_in
            number_current_nodes = len(current_nodes);
            cumulative_probability = [0]*number_current_nodes;
            previous_cumulative_probability = 0;
            for k in range(0,len(current_nodes)):
                if(k == index_pre): #prevent self connection
                    cumulative_probability[k] = previous_cumulative_probability;
                    #previous_cumulative_probability left untouched
                else:
                    cumulative_probability[k] = previous_cumulative_probability + c_in[k]/(np.sum(c_in));
                    previous_cumulative_probability = cumulative_probability[-1];
            
            #select post
            rand = random.random()*previous_cumulative_probability; #scaled due to computer imprecision and self connection prevention
            for i in range(0,number_current_nodes):
                if rand < cumulative_probability[i]:
                    post_node = current_nodes[i];
                    c_in[i] += 1; #post node is receiving the connection
                    conn_j += [post_node];
                    break; #found the pre node following the degree distribution
        
        if random.random() < a:
            if len(buffer_nodes) < 1:
                break;
            new_node = buffer_nodes.pop();
            
            #create cumulative probability array for c_out
            number_current_nodes = len(current_nodes);
            cumulative_probability = [0]*number_current_nodes;
            previous_cumulative_probability = 0;
            for k in range(0,len(current_nodes)):
                cumulative_probability[k] = previous_cumulative_probability + c_out[k]/(np.sum(c_out));
                previous_cumulative_probability = cumulative_probability[-1];
                
            #select old and connect old -> new
            rand = random.random()*previous_cumulative_probability; #scaled due to computer imprecision
            for i in range(0,number_current_nodes):
                if rand < cumulative_probability[i]:
                    target_node = current_nodes[i];
                    #connect new -> old
                    c_out[i] += 1; #old node is giving the connection
                    c_in += [1+din]; #new node is receiving the connection
                    conn_i += [target_node];
                    conn_j += [new_node];
                    break; #found the old node following the degree distribution
    
    #filter for double edges
    new_conn_i = [];
    new_conn_j = [];
    filtered_list = set(zip(conn_i, conn_j));
    for pair in filtered_list:
        new_conn_i += [pair[0]];
        new_conn_j += [pair[1]];
    conn_i = new_conn_i;
    conn_j = new_conn_j;
        
    return conn_i, conn_j;

def randomGeometricConnect(N, d, r, seed):
    """create a geometric directed network where the dimension of the space impact connection density,
        directionality is uniform random for each edge considered.
        :param N: Number of neurons
        :param d: Dimension of the R^d space where the neuron are placed
        :param r: Radius/distance under which an edge is added
        :param seed: seed for random number generator
        :return: conn_i the list of pre-synaptic neuron index to which conn_j the post-synaptic neuron is connected, total number of received input
    """
    random.seed(seed);
    
    conn_i = [];
    conn_j = [];
    
    #place neurons
    vectors = [0 for k in range(0,d)]*N;
    for i in range(0,N):
        vectors[i] = [random.random() for k in range(0,d)];
    
    for i in range(0,N):
        for j in range(0,N):
            #evaluate distance between neuron
            if i != j and np.linalg.norm((np.subtract(vectors[i],vectors[j]))) < r:
                #connect randomly in a way or the other
                if random.random() < 0.5:
                    conn_i += [i];
                    conn_j += [j];
                else:
                    conn_i += [j];
                    conn_j += [i];
    return conn_i, conn_j;

def directedRandomGeometricConnect(N, d, seed):
    """create a geometric directed network where the dimension of the space impact connection density,
        directionality depend on respective range of output for each neuron (uniformly sampled),
        if the neuron can reach its target it will create a connection from neuron to target.
        :param N: Number of neurons
        :param d: Dimension of the R^d space where the neuron are placed
        :param seed: seed for random number generator
        :return: conn_i the list of pre-synaptic neuron index to which conn_j the post-synaptic neuron is connected, total number of received input
    """
    random.seed(seed);
    
    conn_i = [];
    conn_j = [];
    
    #assign a range of output for each neuron
    output_range = [random.random() for k in range(0,N)];
    
    #place neurons
    vectors = [0 for k in range(0,d)]*N;
    for i in range(0,N):
        vectors[i] = [random.random() for k in range(0,d)];
    
    for i in range(0,N):
        for j in range(0,N):
            #evaluate distance between neuron
            if i != j:
                distance = np.linalg.norm((np.subtract(vectors[i],vectors[j]))); 
                if distance <= output_range[i]: #connect i -> j
                    conn_i += [i];
                    conn_j += [j];
                if distance <= output_range[j]: #connect j -> i
                    conn_i += [j];
                    conn_j += [i];
    return conn_i, conn_j;

def wattsStrogatzConnect(N, K, p, seed):
    """create a small-world Watts-Strogatz directed network with high local clustering and low average path short,
        directionality is uniform random for each edge considered.
        :param N: Number of neurons
        :param K: Average connection per Neuron, must be even
        :param p: Probability of rewiring a given connection
        :param seed: seed for random number generator
        :return: conn_i the list of pre-synaptic neuron index to which conn_j the post-synaptic neuron is connected, total number of received input
    """
    random.seed(seed);
    
    conn_i = [];
    conn_j = [];
    random_indexation = random.sample(range(0,N),N); #ensure original neuron indexation independance
    
    #creating small-world annulus
    for i in range(0,N):
        for j in range(0,N):
            number = abs(i-j)%(N-(K/2));
            if number > 0 and number <= (K/2):
                #bidirectional wiring
                conn_i += [random_indexation[i]];
                conn_j += [random_indexation[j]];
                
                conn_i += [random_indexation[j]];
                conn_j += [random_indexation[i]];
    
    #rewiring presynaptic neuron output connection
    for k in range(0,len(conn_i)):
        if random.random() < p: #rewire conn_i[k] to another non-self
            possible_target = list(filter(lambda x : (x != conn_i[k]), range(0,N)));
            
            if(len(possible_target) > 0):
                conn_j[k] = random.choice(possible_target);
                
    #filter for double edges
    new_conn_i = [];
    new_conn_j = [];
    filtered_list = set(zip(conn_i, conn_j));
    for pair in filtered_list:
        new_conn_i += [pair[0]];
        new_conn_j += [pair[1]];
    
    conn_i = new_conn_i;
    conn_j = new_conn_j;
        
    return conn_i, conn_j;

def randomHyperbolicGeometricConnect(N, alpha, R, r, seed):
    """create a hyperbolic geometric directed network where the distribution
        of neuron on an hyperbolic disk impact connection density,
        directionality is uniform random for each edge considered.
        :param N: Number of neurons
        :param alpha: alpha is the related to the inverse of the dispersion along the hyperbolic disk boundary
        :param R: Radius of the hyperbolic disk
        :param r: Radius/distance under which an edge is added
        :param seed: seed for random number generator
        :return: conn_i the list of pre-synaptic neuron index to which conn_j the post-synaptic neuron is connected, total number of received input
    """
    random.seed(seed);
    
    conn_i = [];
    conn_j = [];
    
    #place neurons
    polar = [0 for k in range(0,2)]*N; #radius, theta
    for i in range(0,N):
        polar[i] = [(1/alpha)*np.arccosh(1 + ((np.cosh(alpha*R)-1)*random.random())), random.random()*(2*np.pi)];
    
    for i in range(0,N):
        for j in range(0,N):
            #evaluate distance between neuron
            if i != j and np.sqrt((polar[i][0])**2 + (polar[j][0])**2 - 2*(polar[i][0])*(polar[j][0])*np.cos((polar[i][1]) - (polar[j][1]))) < r:
                #connect randomly in a way or the other
                if random.random() < 0.5:
                    conn_i += [i];
                    conn_j += [j];
                else:
                    conn_i += [j];
                    conn_j += [i];
    return conn_i, conn_j;

def directedRandomHyperbolicGeometricConnect(N, alpha, R, seed):
    """create a hyperbolic geometric directed network where the distribution
        of neuron on an hyperbolic disk impact connection density, directionality
        depend on respective range of output for each neuron (uniformly sampled),
        if the neuron can reach its target it will create a connection from neuron to target.
        :param N: Number of neurons
        :param alpha: alpha is the related to the inverse of the dispersion along the hyperbolic disk boundary
        :param R: Radius of the hyperbolic disk
        :param seed: seed for random number generator
        :return: conn_i the list of pre-synaptic neuron index to which conn_j the post-synaptic neuron is connected, total number of received input
    """
    random.seed(seed);
    
    conn_i = [];
    conn_j = [];
    
    #assign a range of output for each neuron
    output_range = [random.random()*R for k in range(0,N)];
    
    #place neurons
    polar = [0 for k in range(0,2)]*N; #radius, theta
    for i in range(0,N):
        polar[i] = [(1/alpha)*np.arccosh(1 + ((np.cosh(alpha*R)-1)*random.random())), random.random()*(2*np.pi)];
    
    for i in range(0,N):
        for j in range(0,N):
            #evaluate distance between neuron
            if i != j:
                distance = np.sqrt((polar[i][0])**2 + (polar[j][0])**2 - 2*(polar[i][0])*(polar[j][0])*np.cos((polar[i][1]) - (polar[j][1]))); 
                if distance <= output_range[i]: #connect i -> j
                    conn_i += [i];
                    conn_j += [j];
                if distance <= output_range[j]: #connect j -> i
                    conn_i += [j];
                    conn_j += [i];
    return conn_i, conn_j;

def hierarchicalNetworkConnect(N, M, p_cluster, seed):
    """create a hierarchical iterative directed network dividing the neuron into
        densely connected clusters of size M and connecting them by expanding a
        cluster into M free clusters (choose a center at random), directionality
        is random and identical for cluster to cluster
        but completely random for neurons inside of clusters.
        :param N: Number of neurons
        :param M: size of clusters, N must be a power of M (power is the number of iteration + 1)
        :param p_cluster: probability of connection for each edge inside the cluster
        :param seed: seed for random number generator
        :return: conn_i the list of pre-synaptic neuron index to which conn_j the post-synaptic neuron is connected, total number of received input
    """
    random.seed(seed);
    
    conn_i = [];
    conn_j = [];
    
    #create clusters
    buffer = random.sample(range(0,N),N);
    n = int(N/M); #number of clusters
    k_max = int(np.round(np.log(N)/np.log(M))-1); #iteration
    clusters = [[] for k in range(0,n)];
    centers = [0 for k in range(0,n)];
    
    for k in range(0,n):
        cluster = [];
        for m in range(0,M):
            cluster += [buffer.pop()];
        center = random.choice(cluster);
        
        clusters[k] = cluster;
        centers[k] = center;
        
        #connect neurons inside cluster
        for i in range(0,M):
            for j in range(0,M):
                if i != j and random.random() < p_cluster:
                    if random.random() < 0.5:
                        conn_i += [cluster[i]];
                        conn_j += [cluster[j]];
                    else:
                        conn_i += [cluster[j]];
                        conn_j += [cluster[i]];                
        
    #connect clusters
    buffer_index = random.sample(range(0,n),n);
    added_clusters = [buffer_index.pop()];
    for k in range(0,k_max): #iteration
        to_be_added_clusters = [];
        for index in added_clusters: #current clusters
            for m in range(0,M-1): #fractal to new clusters
                to_connect_index = buffer_index.pop();
                to_be_added_clusters += [to_connect_index];
                if random.random() < 0.5: #connect non-center to or from new cluster center
                    for i in clusters[index]:
                        if i != centers[index]:
                            conn_i += [i];
                            conn_j += [centers[to_connect_index]];
                else:
                    for i in clusters[index]:
                        if i != centers[index]:
                            conn_i += [centers[to_connect_index]];
                            conn_j += [i];
                            
        for index_to_add in to_be_added_clusters:
            added_clusters += [index_to_add];
    
    return conn_i, conn_j;

def hierarchicalModularNetworkConnect(N, M, p_cluster, alpha, seed):
    """create a hierarchical modular directed network dividing the neuron into
        densely connected initial clusters of size M and then per iteration connecting
        two of them into one super clusters and connecting their
        respective cluster neurons, 
        intra and inter cluster connections are random.
        :param N: Number of neurons
        :param M: size of initial clusters, N/M must be a power of 2 (power is the number of iteration)
        :param p_cluster: probability of connection for each edge inside the cluster
        :param alpha: related directly to intercluster connection probability (p = alpha/(4^iteration)); 4/(M**2) <= alpha <= 4
        :param seed: seed for random number generator
        :return: conn_i the list of pre-synaptic neuron index to which conn_j the post-synaptic neuron is connected, total number of received input
    """
    random.seed(seed);
    
    conn_i = [];
    conn_j = [];
    
    #create initial clusters
    buffer = random.sample(range(0,N),N);
    n = int(N/M); #number of initial clusters
    k_max = int(np.round(np.log(n)/np.log(2))); #fusion iteration
    initial_clusters = [[] for k in range(0,n)];
    
    for k in range(0,n):
        cluster = [];
        for m in range(0,M):
            cluster += [buffer.pop()];
        
        initial_clusters[k] = cluster;
        
        #connect neurons inside cluster
        for i in range(0,M):
            for j in range(0,M):
                if i != j and random.random() < p_cluster:
                    if random.random() < 0.5:
                        conn_i += [cluster[i]];
                        conn_j += [cluster[j]];
                    else:
                        conn_i += [cluster[j]];
                        conn_j += [cluster[i]];
    
    #create clusters hierarchy
    clusters_index = [random.sample(range(0,n),n)];
    for k in range(0,k_max):
        clusters_buffer = [];
        for m in range(0, len(clusters_index[k])):
            if m%2 == 0:
                clusters_buffer += [(clusters_index[k][m], clusters_index[k][m+1])];
        clusters_index += [clusters_buffer];
    
    #connect clusters
    for k in range(0,k_max):
        for super_cluster in clusters_index[k+1]: #find super-clusters inside hierarchy
            sc1 = super_cluster[0];
            sc2 = super_cluster[1];
            if type(sc1) == int:
                sc1_index = [sc1];
            else:
                sc1_index = sc1;
            if type(sc2) == int:
                sc2_index = [sc2] 
            else:
                sc2_index = sc2;
                
            #unfold hierarchy
            depth = k-1;
            while depth > 0:
                buffer1_index = [];
                buffer2_index = [];
                
                for elem in sc1_index:
                    for e in elem:
                        buffer1_index += [e];
                        
                for elem in sc2_index:
                    for e in elem:
                        buffer2_index += [e];
                    
                sc1_index = buffer1_index;
                sc2_index = buffer2_index;
                depth -= 1;
            #connect sc1 to/from sc2
            for index1 in sc1_index:
                for index2 in sc2_index:
                    for i in initial_clusters[index1]:
                        for j in initial_clusters[index2]:
                            if random.random() < alpha/(4**(k+1)):
                                if random.random() < 0.5:
                                    conn_i += [i];
                                    conn_j += [j];
                                else:
                                    conn_i += [j];
                                    conn_j += [i];
    
    return conn_i, conn_j;

#DEPRECATED
#identical to semirandConnect
def directedDRegularConnect(N, d, seed):
    """randomly connect post-synaptic neurons to d neurons without self-connection.
        The following graph is regular in regards to receiving connection.
        :param N: Number of neurons
        :param d: Number of connections received by post-synaptic neurons
        :param seed: seed for random number generator
        :return: conn_i the list of pre-synaptic neuron index to which conn_j the post-synaptic neuron is connected
    """
    random.seed(seed);
    
    conn_i = [];
    conn_j = [];
    for j in range(0,N):
		# sample d number of neuron indices without the index j
        preIdxsE = random.sample(list(filter(lambda x : x != j, range(0, N))), d);
        conn_i += preIdxsE;
        conn_j += [j]*d;
    return conn_i, conn_j;