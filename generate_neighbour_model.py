# -*- coding: utf-8 -*-
"""
Created on Wed Apr  1 22:59:50 2020

@author: arthu
"""

import parameters;
import utility;

# SIMULATION PARAMETERS:
connect_model = parameters._connect_model;
N_true = parameters._N_brunnel;
downscale_factor = parameters._downscale_factor;
replication = parameters._replication;
# RESAMPLING PARAMETERS:
count = parameters._count;
resample_model = parameters._resample_model;
percentage_usimple = parameters._percentage_usimple;

def resample(seed, cseed):
    
    if resample_model == 'dsimple':
        utility.downsampleConnectivity('data/connectivity/base_{}_{}.txt'.format(connect_model, cseed), count, seed);
    elif resample_model == 'usimple':
        utility.upsampleConnectivity('data/connectivity/base_{}_{}.txt'.format(connect_model, cseed), int(N_true/downscale_factor), count, percentage_usimple, seed);
    else:
        print('unknown resample model : {}'.format(resample_model));
    
        
# main
if __name__=='__main__':
    
    s = parameters._boostrap_seed;
    
    for seedling in range(0,replication):
        resample(s,seedling);