# -*- coding: utf-8 -*-
"""
Created on Wed Mar  4 18:23:56 2020

@author: arthu
"""
import matplotlib.pyplot as plt
import numpy as np;
import utility;
import parameters;

"""create a plot of the betti curves from the ripser data
"""

#file parameters
connect_models = parameters._connect_models;
#processing parameters
max_threshold = parameters._max_threshold;
measure_types = parameters._measure_types;
_d = parameters._binning_step;
#drawing parameters
max_dimension = parameters._max_dimension;

#DEPRECATED
def DrawBettiCurves(g, inp, connect_model, seed, cseed):
    for measure_type in measure_types:
        name = 'data/ph/simulation_{}_{}_i={}_g={}_s={}_{}_ripser.txt'.format(connect_model, cseed, inp, g, seed, measure_type);
        
        betti_curves = [];
        
        #calculate curve
        betti_curves = utility.sampledBettiCurves(name, _d, max_threshold);
        
        #Combined
        plt.figure(figsize=(12.0, 9.0)) # in inches!

        for dim in range(0,(max_dimension + 1)):
            plt.plot(np.linspace(0, max_threshold, (int(max_threshold/_d) + 1)),betti_curves[dim], label = r'$\beta_{}$'.format(dim));
            
        # draw and save plot
        plt.xlabel('filtration threshold');
        plt.ylabel('Betti number');
        plt.title(r'$\beta_0,\ \beta_1 \ curves : {}_{} \ {} \ (i={}, g={}) \ seed={}$'.format(connect_model, cseed, measure_type, inp, g, seed));
        plt.legend();
        plt.savefig('data/betti_curves/combined_{}_{}_i={}_g={}_s={}_{}.png'.format(connect_model, cseed, inp, g, seed, measure_type), format="png", dpi=300);
        plt.close();
        
        #Separated lin and log
        #plt.figure(figsize=(24.0, 18.0)) # in inches!
        #
        #for dim in range(0,(max_dimension + 1)):
        #    plt.subplot(221 + dim);
        #    
        #    plt.plot(np.linspace(0, max_threshold, (int(max_threshold/_d) + 1)),betti_curves[dim], label = r'$\beta_{}$'.format(dim));
        #       
        #   # draw and save plot
        #   plt.xlabel('filtration threshold');
        #   plt.ylabel('Betti number');
        #   plt.title(r'$\beta_{} \ curve : {}_{} \ {} \ (i={}, g={}) \ seed={}$'.format(dim, connect_model, cseed, measure_type, inp, g, seed));
        #   plt.legend();
        #   
        #   plt.subplot(223 + dim);
        #   
        #   plt.plot(np.linspace(0, max_threshold, (int(max_threshold/_d) + 1)),np.log(betti_curves[dim]+1), label = r'$\beta_{}$'.format(dim));
        #        
        #    # draw and save plot
        #    plt.xlabel('filtration threshold');
        #    plt.ylabel(r'$\ln(Betti number + 1)$');
        #    plt.title(r'$\beta_{} \ curve : {}_{} {} (i={}, g={}) seed={}$'.format(dim, connect_model, cseed, measure_type, inp, g, seed));
        #    plt.legend();
        #
        #plt.savefig('data/betti_curves/separated_{}_{}_i={}_g={}_s={}_{}.png'.format(connect_model, cseed, inp, g, seed, measure_type), format="png", dpi=300);
        #plt.close();
        
def DrawAllBettiCurves(g, inp, connect_model):
    for measure_type in measure_types:
        #Superposed
        plt.figure(0, figsize=(24.0, 9.0)) # in inches!
        
        for cseed in range(0,1): #replication
            for seed in range(0,parameters._replication):
                name = 'data/ph/simulation_{}_{}_i={}_g={}_s={}_{}_ripser.txt'.format(connect_model, cseed, inp, g, seed, measure_type);
                
                betti_curves = [];
                
                #calculate curve
                betti_curves = utility.sampledBettiCurves(name, _d, max_threshold);
                
                #Superposed
                plt.figure(0)
                    
                for dim in range(0,(max_dimension + 1)):
                    plt.subplot(121 + dim);
                    
                    plt.plot(np.linspace(0, max_threshold, (int(max_threshold/_d) + 1)),betti_curves[dim], label = r'seed = {}'.format(seed));
                    
                #Combined
                plt.figure(figsize=(12.0, 9.0)) # in inches!
        
                for dim in range(0,(max_dimension + 1)):
                    plt.plot(np.linspace(0, max_threshold, (int(max_threshold/_d) + 1)),betti_curves[dim], label = r'$\beta_{}$'.format(dim));
                    
                # draw and save combined plot
                plt.xlabel('filtration threshold');
                plt.ylabel('Betti number');
                plt.title(r'$\beta_0,\ \beta_1 \ curves : {}_{} \ {} \ (i={}, g={}) \ seed={}$'.format(connect_model, cseed, measure_type, inp, g, seed));
                plt.legend();
                plt.savefig('data/betti_curves/combined_{}_{}_i={}_g={}_s={}_{}.png'.format(connect_model, cseed, inp, g, seed, measure_type), format="png", dpi=300);
                plt.close();
                        
            # draw and save superposed plot
            plt.figure(0)
                    
            for dim in range(0,(max_dimension + 1)):
                plt.subplot(121 + dim);
                
                plt.xlabel('filtration threshold');
                plt.ylabel('Betti number');
                plt.title(r'$\beta_{} \ curve : {}_{} \ {} \ (i={}, g={})$'.format(dim, connect_model, cseed, measure_type, inp, g));
                plt.legend();
                        
            plt.savefig('data/betti_curves/superposed_{}_{}_i={}_g={}_{}.png'.format(connect_model, cseed, inp, g, measure_type), format="png", dpi=300);
            plt.close();
        
try:  
    inp = parameters._input;
    g = parameters._g;

    for cm in connect_models:
        DrawAllBettiCurves(g, inp, cm);
        #for cseedling in range(0,1): #replication
        #    for seedling in range(0,parameters._replication):
        #        pass
        #        DrawBettiCurves(g, inp, cm, seedling, cseedling);
    
except Exception as e:
  print('Error:')
  print(str(e))