# -*- coding: utf-8 -*-
"""
Created on Wed Mar 11 11:42:27 2020

@author: arthu
"""
import parameters;

"""produce a file containing the commands to use ripser in /scripts from ubuntu terminal (or similar)
    you need to then run the following command in your terminal
    
    dos2unix runRipser.sh
    ./runRipser.sh
    
    Warning : the dos2unix is necessary since the file generated from this script is not encoded correctly for ubuntu shell,
    so unexpected characters are added to the file extension without this pre-treatment
"""

# ripser parameters
runRipser_name = parameters._runRipser_name
max_threshold = parameters._max_threshold;
max_dimension = parameters._max_dimension;
connect_models = parameters._connect_models;
measure_types = parameters._measure_types;

def GenerateRunRipser(g, inp):
    output = open(runRipser_name,'w');
    
    #setup automated shell script file
    output.write('#!/bin/sh');
    
    #write commands
    for connect_model in connect_models:
        for cseedling in range(0,1): #replication
            for seedling in range(0,parameters._replication):
                for measure_type in measure_types:
                    output.write('\n');
                    output.write(GenerateCommand(g, inp, connect_model, measure_type, seedling, cseedling));

    output.close();

def GenerateCommand(g, inp, connect_model, measure_type, seed, cseed):
    path_to_input = 'data/similarity/simulation_{}_{}_i\={}_g\={}_s\={}_{}.lower_distance_matrix'.format(connect_model, cseed, inp, g, seed, measure_type);
    path_to_output = 'data/ph/simulation_{}_{}_i\={}_g\={}_s\={}_{}_ripser.txt'.format(connect_model, cseed, inp, g, seed, measure_type);
    
    return '../../ripser/ripser --format lower-distance --dim {} --threshold {} {} > {}'.format(max_dimension, max_threshold, path_to_input, path_to_output); 

try:  
    inp = parameters._input;
    g = parameters._g;
    
    GenerateRunRipser(g, inp);
    
except Exception as e:
  print('Error:')
  print(str(e))