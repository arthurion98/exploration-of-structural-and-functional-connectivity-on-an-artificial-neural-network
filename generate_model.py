# -*- coding: utf-8 -*-
"""
Created on Wed Apr  1 15:00:06 2020

@author: arthu
"""

import parameters;
import utility;

# SIMULATION PARAMETERS:
downscale = parameters._downscale_factor;
connectivity = parameters._connectivity_ratio;
N_true = parameters._N_brunnel;
f_exc = parameters._fraction_excitatory;
connect_models = parameters._connect_models;
replication = parameters._replication;
# CONNECTION MODEL PARAMETERS:
m0_bam = parameters._initial_bam;
proba_dbam = parameters._probability_dbam;
dim_rgg = parameters._dimension_rgg;
rad_rgg = parameters._radius_rgg;
proba_ws = parameters._probability_ws;
alpha_rhgg = parameters._alpha_rhgg;
rad_rhgg = parameters._radius_rhgg;
m_hn = parameters._cluster_size_hn;
proba_hn = parameters._probability_hn;
m_hmn = parameters._cluster_init_hmn;
alpha_hmn = parameters._alpha_hmn;

N = int(N_true / downscale)		# number of Neuron
C = int(connectivity * N)		# number of connexions per neuron
Ce = int(C * f_exc)             # number of excitatory connexions
NE = int(N * f_exc)             # Number of excitatory cells

def generate(connect_model, seed):
    
    # generate connectivity based on model
    if connect_model == 'default':
        conn_i, conn_j = utility.defaultConnect(N, NE, C, Ce, seed);
    elif connect_model == 'rand':
        conn_i, conn_j = utility.randomConnect(N, seed);
    elif connect_model == 'semirand':
        conn_i, conn_j = utility.semirandomConnect(N, C, seed);
    elif connect_model == 'erdosrenyi':
        conn_i, conn_j = utility.erdosRenyiConnect(N, C, seed);   
    elif connect_model == 'bam':
        conn_i, conn_j, tot = utility.barabasiAlbertConnect(N, m0_bam, seed);
    elif connect_model == 'dbam':
        conn_i, conn_j = utility.directedBarabasiAlbertConnect(N, proba_dbam, proba_dbam, proba_dbam, Ce, 0, seed);
    elif connect_model == 'rgg':
        conn_i, conn_j = utility.randomGeometricConnect(N, dim_rgg, rad_rgg, seed);
    elif connect_model == 'drgg':
        conn_i, conn_j = utility.directedRandomGeometricConnect(N, dim_rgg, seed);
    elif connect_model == 'ws':
        conn_i, conn_j = utility.wattsStrogatzConnect(N, C, proba_ws, seed);
    elif connect_model == 'rhgg':
        conn_i, conn_j = utility.randomHyperbolicGeometricConnect(N, alpha_rhgg, rad_rhgg, rad_rgg, seed);
    elif connect_model == 'drhgg':
        conn_i, conn_j = utility.directedRandomHyperbolicGeometricConnect(N, alpha_rhgg, rad_rhgg, seed);
    elif connect_model == 'hn':
        conn_i, conn_j = utility.hierarchicalNetworkConnect(N, m_hn, proba_hn, seed);
    elif connect_model == 'hmn':
        conn_i, conn_j = utility.hierarchicalModularNetworkConnect(N, m_hmn, proba_hn, alpha_hmn, seed);
    elif connect_model == 'ddreg':
        conn_i, conn_j = utility.directedDRegularConnect(N, C, seed);
    else:
        print('unknown model : {}'.format(connect_model));
    
    # save connectivity data
    utility.saveRawConnectivity(conn_i, conn_j, 'data/connectivity/base_{}_{}.txt'.format(connect_model, seed));
    
# main
if __name__=='__main__':
    
    #cs = parameters._connectivity_seed;
    
    for cm in connect_models:
        for seedling in range(0,1): #replication
            generate(cm, seedling);