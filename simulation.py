# -*- coding: utf-8 -*-
"""
Created on Fri Feb 28 11:36:59 2020

@author: arthu
@original: JB
"""

import numpy as np;
import brian2 as br;
import random;
import parameters;
import utility;

"""simulate a brunnel network and save the firing rate and spike train as data
"""

# SIMULATION PARAMETERS:
sim_time = parameters._simulation_time;
sim_step = parameters._simulation_step;
downscale = parameters._downscale_factor;
connectivity = parameters._connectivity_ratio;
syn_delay = parameters._synaptic_delay;
Vm_jump_after_EPSP = parameters._Vm_jump_after_EPSP;
N_true = parameters._N_brunnel;
f_exc = parameters._fraction_excitatory;
connect_models = parameters._connect_models;
# NEURON PARAMETERS:
membrane_time_cst = parameters._membrane_time_cst;
offset = parameters._offset;
Vreset = parameters._Vreset
threshold = parameters._threshold;
refractory_period = parameters._refractory_period;
# CONNECTION MODEL PARAMETERS:
m0_bam = parameters._initial_bam;
proba_dbam = parameters._probability_dbam;
dim_rgg = parameters._dimension_rgg;
rad_rgg = parameters._radius_rgg;
proba_ws = parameters._probability_ws;
alpha_rhgg = parameters._alpha_rhgg;
rad_rhgg = parameters._radius_rhgg;
m_hn = parameters._cluster_size_hn;
proba_hn = parameters._probability_hn;
m_hmn = parameters._cluster_init_hmn;
alpha_hmn = parameters._alpha_hmn;

def Simulate(g, inp, connect_model, seed, cseed):
    """Simulation of the Brunel network with
        relative strength between IPSP and EPSP g and 
        external input parameter inp.
        Inp=1 is the minimum amount of external stimulation to observe sustained 
        activity in the full network (12 500 neurons).
    """

    br.set_device('cpp_standalone')
    br.prefs.codegen.target = 'weave'
  
	# NETWORK INITIALIZATION
    np.random.seed(seed) # set seed for reproducibility of simulations
    random.seed(seed) # set seed for reproducibility of simulations

	# =============================================================================
	# PARAMETERS
	# Simulation parameters
    simdt = sim_step*br.ms
    simtime = sim_time*br.second
    br.defaultclock.dt = simdt         # Brian's default sim time step
    dt = br.defaultclock.dt/br.second  
  
  	# scaling parameter to the true on of Brunel 2000
    delay = syn_delay * br.ms                   # Synaptic delay
    J = Vm_jump_after_EPSP * downscale * br.mV  # jump of membrane potential after EPSP
  
  	# Network numbers parameters
    N = int(N_true / downscale)		# number of Neuron
    C = int(connectivity * N)		# number of connexions per neuron
    sparseness = C/float(N)
    Ce = int(C * f_exc)             # number of excitatory connexions
    NE = int(N * f_exc)             # Number of excitatory cells
    NI = N-NE                       # Number of inhibitory cells 
    CE_true = int(N_true * connectivity * f_exc)
  
  	# Neurons parameters
    tau = membrane_time_cst * br.ms                 # membrane time constant
    mu_0 = offset * br.mV                  # offset to the membrane
    Vr = mu_0 + Vreset * br.mV           # potential of reset after spiking
    theta = mu_0 + threshold * br.mV  	      # Spiking threshold
    taurefr = refractory_period * br.ms               # time constant of refractory period
  
  	# Synapse parameters
    g = float(g)                    # relative strength between IPSP and EPSP
  
  	# parameter of external population
    Inp = float(inp)                # coefficient multiplying nu_theta
    nu_theta = theta/(Ce * J * tau) # minimal required input frequency for firing
  
  	# =============================================================================
  	# INITIALIZE NEURONS GROUP
  	# Main group
    eqs_neurons= br.Equations('''
  	dV/dt = (-V +  mu_0)/tau : volt (unless refractory)
  	''')
    Group=br.NeuronGroup(
        N, model=eqs_neurons,\
        threshold='V>=theta',
        reset='V=Vr',\
        refractory=taurefr, method='euler')
    Group.V = np.linspace(mu_0/br.mV-20,theta/br.mV,N)*br.mV
  
  	# external group
  
  	# =============================================================================
  	# SYNAPSES DEFINITION AND CONNEXIONS
  
  	# Main group synapses
    sparseness_e = Ce/float(NE);
    sparseness_i = (1-f_exc)*C/float(NI);
    con = br.Synapses(Group,Group,'w:volt',on_pre='V_post += w',method='euler');
    
    # retrieve connectivity data
    conn_i, conn_j = utility.extractRawConnectivity('data/connectivity/base_{}_{}.txt'.format(connect_model, cseed));
    
    # connect j-th presynaptically to i-th pre-synaptic neuron
    # choose the synapses object based on whether pre-synaptic neuron is exc or inh
    con.connect(i=conn_i, j=conn_j);
    con.delay = delay;
    con.w['i<NE'] = J;
    con.w['i>=NE'] = -g*J;
  
    # EXTERNAL POPULATION
    #correct external population firing rate for downscaling
    if g > 4:
        approx_rate = ((Inp - 1) * nu_theta)/(g*0.25 - 1)/N
        rate_ext_0 =  Ce * Inp * nu_theta
        rate_balance = Ce * ((1/downscale) - 1) *\
                        (Inp * nu_theta + approx_rate*(1 + 0.25*g**2)) /\
                        (1 + g**2)
        rate_corrected = (rate_ext_0 + rate_balance)/Ce
    else:
        rate_corrected=Inp*nu_theta
    print(Inp*nu_theta, rate_corrected)
  
    PI = br.PoissonInput(Group, 'V', N=Ce, rate=rate_corrected, weight=J)	

	# =============================================================================
	# SIMULATION

	#np.random.seed(seed)
	#random.seed(seed)

	# Setting up monitors
    M = br.SpikeMonitor(Group)
    LFP = br.PopulationRateMonitor(Group)
  
    br.run(simtime,report='text')
  
    # saving population firing rate and spike trains
    fr = [LFP.t/br.ms, LFP.smooth_rate(window='flat', width=0.5*br.ms)/br.Hz]
    sp = M.spike_trains()
    br.device.reinit()
    return sp, fr

if __name__=='__main__':
    try:
        inp = parameters._input;
        g = parameters._g;
        #s = parameters._seed;
        #cs = parameters._connectivity_seed;
  
        #input_ = i#[1,2,3,4]
        #g_ = g#[2,3,3.5,4,4.5,5,6,7,8]
  
        #  for inp in input_:
        #    for g in g_:
        #for s in range(10):
        for cm in connect_models:
            for cseedling in range(0,1): #replication
                for seedling in range(0,parameters._replication):
                    #print('seed: ', s)
                    sp, fr = Simulate(g, inp, cm, seed=seedling, cseed=cseedling)
                    for k, d in sp.items():
                        sp[k] = d/br.second
    
                    d = {'sp': sp, 'fr': fr}
                    np.save('data/simulation/simulation_{}_{}_i={}_g={}_s={}'.format(cm, cseedling, inp, g, seedling), d)
                
    except Exception as e:
        print('Error:')
        print(str(e))    
