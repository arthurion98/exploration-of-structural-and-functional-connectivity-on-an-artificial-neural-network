# -*- coding: utf-8 -*-
"""
Created on Fri Feb 28 15:15:55 2020

@author: arthu
@original: JB
"""
#I had to setup manually pyspike and therefore anaconda doesn't by default link to it
#so this portion of code is just there to create a linker to my PySpike library
import sys;
sys.path.append('H:/PySpike');
#sys.path.append('C:/ProgramData/PySpike');
#END

import numpy as np;
import pyspike as spk;
import utility;
import parameters;

"""from the simulation data files, compute the different similarity measure and save them as lower distance matrix
"""
#file parameters
connect_models = parameters._connect_models;
#spike train parameters
t_end = parameters._simulation_time;

def Measure(g, inp, connect_model, seed, cseed):
    #import data
    d = np.load('data/simulation/simulation_{}_{}_i={}_g={}_s={}.npy'.format(connect_model, cseed, inp, g, seed), allow_pickle=True).item()
    sp = d['sp']
    
    #create spike trains list  
    spike_list = []
    for train in sp:
        spike_list.append(spk.SpikeTrain(list(sp[train]), (0, t_end), is_sorted=False))
        
    #SPIKE synchronicity
    sync_dist = spk.spike_sync_matrix(spike_list)
    for i in range(sync_dist.shape[0]):
        sync_dist[i,i] = 1
    
    #SPIKE distance
    spike_dist = spk.spike_distance_matrix(spike_list)
  
    #Save
    utility.saveMeasureLowerMatrix(1-sync_dist, 'data/similarity/simulation_{}_{}_i={}_g={}_s={}_sync.lower_distance_matrix'.format(connect_model, cseed, inp, g, seed))
    utility.saveMeasureLowerMatrix(spike_dist, 'data/similarity/simulation_{}_{}_i={}_g={}_s={}_dist.lower_distance_matrix'.format(connect_model, cseed, inp, g, seed))
  #utility.saveMeasureFullMatrix(1-sync_dist, 'data/similarity/simulation_i={}_g={}_s=_{}_sync.txt'.format(inp, g, s))
  #utility.saveMeasureFullMatrix(spike_dist, 'data/similarity/simulation_i={}_g={}_s=_{}_dist.txt'.format(inp, g, s))
#  utils.Weight2txt(1-sync_dist, 'similarity/brunel_inp={}_g={}_seed_{}_sync.txt'.format(inp, g, s))
#  utils.Weight2txt(spike_dist, 'similarity/brunel_inp={}_g={}_seed_{}_dist.txt'.format(inp, g, s))
  
  #Correlation computing
  #corr = utils.Correlation_matrice(sp, interval=(1, 20), bin_by_sec=500, tau=1)
  #for i in range(corr.shape[0]):
  #  corr[i, i] = 1 
  #utils.Weight2txt(1-corr, 'similarity/brunel_inp={}_g={}_seed_{}_corr.txt'.format(inp, g, s))

try:  
    inp = parameters._input;
    g = parameters._g;

    for cm in connect_models:
        for cseedling in range(0,1): #replication
            for seedling in range(0,parameters._replication):
                Measure(g, inp, cm, seedling, cseedling);
            
except Exception as e:
  print('Error:')
  print(str(e))
    