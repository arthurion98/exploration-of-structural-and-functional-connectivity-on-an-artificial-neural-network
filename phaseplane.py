# -*- coding: utf-8 -*-
"""
Created on Mon Jun  1 09:55:14 2020

@author: Tâm Nguyên
"""
import numpy as np;
import utility;
import matplotlib.pyplot as plt;
import parameters;

#file parameters
connect_models = parameters._connect_models;

def DrawPhasePlane(connect_model, cseed):
    
    #import Dynamics Label and phase plane
    DL = utility.readDynamicsLabel('data/measures/dynamics_label.txt');
    
    full_phase_plane = DL[connect_model];
    
    colors = {}
    colors['SR'] = 'dodgerblue';
    colors['SR_SI'] = 'lightskyblue';
    colors['SR_0'] = 'lightskyblue';
    colors['AI'] = 'coral';
    colors['Alt'] = 'yellowgreen';
    colors['Alt>SR_0'] = 'aquamarine';
    colors['Alt<>SR_0'] = 'lawngreen';
    
    added_regimes_name = set();
    
    plt.figure(figsize=(12.0, 12.0));
    for g in [2,4,6]:
        for inp in [1,2,3,4]:
            #draw square
            regimes = full_phase_plane[int(g/2)-1][inp-1];
            regimes_name = set(regimes);
            
            if len(regimes_name) == 1: #stable regime
                regime_max = regimes[0];
                plt.fill([g-1,g-1,g+1,g+1],[inp-0.5,inp+0.5,inp+0.5,inp-0.5], color=colors[regime_max]);
                if regime_max not in added_regimes_name:
                    plt.plot([],[], color=colors[regime_max], label=r'${}$'.format(regime_max)); #proxy legend
                    added_regimes_name.add(regime_max); #no double legend
            if len(regimes_name) > 1: #unstable regime
                regimes_count = {}
                for name in regimes_name:
                    regimes_count[name] = 0;
                for regime in regimes:
                    regimes_count[regime] += 1;
                
                regimes_ordered = [k for k, v in sorted(regimes_count.items(), key=lambda item: item[1], reverse=True)]
                regime_max = regimes_ordered[0];
                regime_sec = regimes_ordered[1];
                
                plt.fill([g-1,g-1,g+1],[inp-0.5,inp+0.5,inp+0.5], color=colors[regime_max]);
                plt.fill([g-1,g+1,g+1],[inp-0.5,inp-0.5,inp+0.5], color=colors[regime_sec]);
                if regime_max not in added_regimes_name:
                    plt.plot([],[], color=colors[regime_max], label=r'${}$'.format(regime_max)); #proxy legend
                    added_regimes_name.add(regime_max); #no double legend
                if regime_sec not in added_regimes_name:
                    plt.plot([],[], color=colors[regime_sec], label=r'${}$'.format(regime_sec)); #proxy legend
                    added_regimes_name.add(regime_sec); #no double legend
    plt.xlabel('g');
    plt.ylabel('input');
    plt.title(r'$phase \ plane : {}_{}$'.format(connect_model, cseed));
    plt.legend();
    plt.savefig('data/phase_plane/dynamics_{}_{}.png'.format(connect_model, cseed), format="png", dpi=300);
    plt.close();
    
try:
    for cm in connect_models:
        for cseedling in range(0,1): #replication
            DrawPhasePlane(cm, cseedling);
                        
except Exception as e:
    print('Error:')
    print(str(e))    
            

