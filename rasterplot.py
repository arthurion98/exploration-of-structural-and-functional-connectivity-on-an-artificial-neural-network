# -*- coding: utf-8 -*-
"""
Created on Fri May 15 12:45:51 2020

@author: Tâm Nguyên
"""
import numpy as np;
import matplotlib.pyplot as plt;
import parameters;

"""create a rasterplot of spikes from the simulation data
"""

#file parameters
connect_models = parameters._connect_models;
#simulation parameters
N = int(parameters._N_brunnel/parameters._downscale_factor);
#rasterplot parameters
t_start = parameters._t_start;
t_end = parameters._t_end;
timestep = parameters._hist_bin_step;

def DrawRasterPlot(g, inp, connect_model, seed, cseed, t_start, t_end):
    d = np.load('data/simulation/simulation_{}_{}_i={}_g={}_s={}.npy'.format(connect_model, cseed, inp, g, seed), allow_pickle=True).item();
    sp = d['sp'];

    bins = np.arange(t_start, t_end+timestep, timestep);
    
    color_code = [[0,0,0] for k in range(0,1000)];
    
    spktrain_to_plot = [];
    spkhist_to_plot = [];
    for i in range(0,1000):
        list_temp = list(filter(lambda x : (x >= t_start) and (x < t_end), sp[i]));
        spktrain_to_plot.append(list_temp);
        spkhist_to_plot.extend(list_temp);
    
    #plt.figure(figsize=(24.0, 24.0));
    plt.figure(figsize=(48.0, 12.0));
    
    plt.subplot(211);
    plt.eventplot(spktrain_to_plot, colors = color_code);
    plt.xlabel('time [s]');
    plt.ylabel('neuron index');
    plt.title(r'$neural \ spikes \ rasterplot : {}_{} \ (i={}, g={}) \ seed={}$'.format(connect_model, cseed, inp, g, seed));
    
    plt.subplot(212);
    plt.hist(spkhist_to_plot, bins);
    plt.xlabel('time [s]');
    plt.ylabel('spike count');
    plt.title(r'$population \ firing \ histogram : {}_{} \ (i={}, g={}) \ seed={}$'.format(connect_model, cseed, inp, g, seed));
    
    #plt.savefig('data/rasterplot/all_{}_{}_i={}_g={}_s={}.png'.format(connect_model, cseed, inp, g, seed), format="png", dpi=300);
    plt.savefig('data/rasterplot/long_{}_{}_i={}_g={}_s={}.png'.format(connect_model, cseed, inp, g, seed), format="png", dpi=300);
    plt.close();
    
try:  
    inp = parameters._input;
    g = parameters._g;

    for cm in connect_models:
        for cseedling in range(0,1): #replication
            for seedling in range(0,parameters._replication):
                DrawRasterPlot(g, inp, cm, seedling, cseedling, t_start, t_end);
                            
except Exception as e:
    print('Error:')
    print(str(e))    