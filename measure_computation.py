# -*- coding: utf-8 -*-
"""
Created on Fri May  8 18:07:58 2020

@author: Tâm Nguyên
"""
import numpy as np;
import utility;
import parameters;
from scipy.integrate import simps;
import bct;

"""from the connectivity and simulation data files, compute different connectivity measures
    and save them as features
"""
#file parameters
connect_models = parameters._connect_models;
#processing parameters
max_threshold = parameters._max_threshold;
distance_types = parameters._measure_types;
_d = parameters._binning_step;
#drawing parameters
max_dimension = parameters._max_dimension;
#datasaving parameters
dynamics_key = parameters._dynamics_key;
model_key = parameters._model_key;

def ConnectivityMeasure(connect_model, cseed):
    """compute a set of measures on the connectivity matrix and saves them
        in a dictionary.
        :param connect_model: graph type used for the simulation
        :param cseed: seed used for the graph generation
        :return: a dictionary with measures name as key and their corresponding values
    """
    measures = {};
    
    #extract connectivity matrix
    G = utility.extractMatrixFromConnectivity('data/connectivity/base_{}_{}.txt'.format(connect_model, cseed), 1000);
    
    #connectivity model (label)
    measures[model_key] = connect_model;
    
    #degree
    din, dout, dtot = bct.degrees_dir(G);
    
    measures['avg_din'] = np.mean(din);
    measures['std_din'] = np.std(din);
    #measures['min_din'] = min(din);
    #measures['max_din'] = max(din);
    
    measures['avg_dout'] = np.mean(dout);
    measures['std_dout'] = np.std(dout);
    #measures['min_dout'] = min(dout);
    #measures['max_dout'] = max(dout);
    
    measures['avg_dtot'] = np.mean(dtot);
    measures['std_dtot'] = np.std(dtot);
    #measures['min_dtot'] = min(dtot);
    #measures['max_dtot'] = max(dtot);
    
    #distance
    L, efficiency, ecc, radius, diameter = bct.charpath(bct.distance_bin(G));
    
    measures['char_path'] = L;
    measures['efficiency'] = efficiency;
    measures['radius'] = radius;
    measures['diameter'] = diameter;
    
    #modularity
    ci, Q = bct.modularity_dir(G);
    
    measures['modularity'] = Q;
    
    #core / assortativity
    ac_oi = bct.assortativity_bin(G, flag = 1);
    ac_io = bct.assortativity_bin(G, flag = 2);
    ac_oo = bct.assortativity_bin(G, flag = 3);
    ac_ii = bct.assortativity_bin(G, flag = 4);
    
    measures['assortativity_coeff_oi'] = ac_oi;
    measures['assortativity_coeff_io'] = ac_io;
    measures['assortativity_coeff_oo'] = ac_oo;
    measures['assortativity_coeff_ii'] = ac_ii;
    
    #centrality
    between = bct.betweenness_bin(G);
    participation_in = bct.participation_coef(G, ci, degree='in');
    participation_out = bct.participation_coef(G, ci, degree='out');
    fc, FC, total_flo = bct.flow_coef_bd(G);
    
    measures['avg_betweeness_centrality'] = np.mean(between);
    measures['avg_participation_coeff_in'] = np.mean(participation_in);
    measures['avg_participation_coeff_out'] = np.mean(participation_out);
    measures['avg_flow_coeff'] = FC;
    
    #clustering
    clustering = bct.clustering_coef_bd(G);
    transitivity = bct.transitivity_bd(G);
    
    measures['avg_clustering_coeff'] = np.mean(clustering);
    measures['transitivity'] = transitivity;
    
    #print(measures);
    
    return measures;
    
def FunctionalMeasure(g, inp, connect_model, seed, cseed):
    """compute a set of measures on the functional matrix and saves them
        in a dictionary.
        :param g: parameter g of the phase plane
        :param inp: parameter input of the phase plane
        :param connect_model: graph type used for the simulation
        :param distance_type: distance type used to generate the functional matrix
        :param seed: seed used for the simulation
        :param cseed: seed used for the graph generation
        :return: a dictionary with measures name as key and their corresponding values
    """
    measures = {};
    DL = utility.readDynamicsLabel('data/measures/dynamics_label.txt');

    #connectivity model (label)
    measures[model_key] = connect_model;
    
    #dynamics (label)
    measures[dynamics_key] = DL[connect_model][int(g/2)-1][inp-1][seed];
    
    for distance_type in distance_types:
        #extract functional matrix
        G = utility.extractMatrixFromSimulationLowerMatrix('data/similarity/simulation_{}_{}_i={}_g={}_s={}_{}.lower_distance_matrix'.format(connect_model, cseed, inp, g, seed, distance_type), 1000);
        
        #strength
        strength = bct.strengths_und(G);
        
        measures['{}_avg_strength'.format(distance_type)] = np.mean(strength);
        measures['{}_std_strength'.format(distance_type)] = np.std(strength);
                
        #distance
        D, B = bct.distance_wei(G)
        L, efficiency, ecc, radius, diameter = bct.charpath(D);
        
        measures['{}_char_path'.format(distance_type)] = L;
        measures['{}_efficiency'.format(distance_type)] = efficiency;
        measures['{}_radius'.format(distance_type)] = radius;
        measures['{}_diameter'.format(distance_type)] = diameter;
        
        #modularity
        ci, Q = bct.modularity_und(G);
        
        measures['{}_modularity'.format(distance_type)] = Q;
        
        #core / assortativity
        ac = bct.assortativity_wei(G, flag = 0);
        
        measures['{}_assortativity_coeff'.format(distance_type)] = ac;
        
        #centrality
        between = bct.betweenness_wei(G);
        participation = bct.participation_coef(G, ci, degree='undirected');
        
        measures['{}_avg_betweeness_centrality'.format(distance_type)] = np.mean(between);
        measures['{}_avg_participation_coeff'.format(distance_type)] = np.mean(participation);
        
        #clustering
        clustering = bct.clustering_coef_wu(G);
        transitivity = bct.transitivity_wu(G);
        
        measures['{}_avg_clustering_coeff'.format(distance_type)] = np.mean(clustering);
        measures['{}_transitivity'.format(distance_type)] = transitivity;
        
    return measures;
    
def AlgebraicTopologyMeasure(g, inp, connect_model, seed, cseed):
    """compute a set of measures on the betti curves and saves them
        in a dictionary.
        :param g: parameter g of the phase plane
        :param inp: parameter input of the phase plane
        :param connect_model: graph type used for the simulation
        :param distance_type: distance type used to generate the functional matrix
        :param seed: seed used for the simulation
        :param cseed: seed used for the graph generation
        :return: a dictionary with measures name as key and their corresponding values
    """
    measures = {};
    
    for distance_type in distance_types:
        
        #calculate betti curves
        name = 'data/ph/simulation_{}_{}_i={}_g={}_s={}_{}_ripser.txt'.format(connect_model, cseed, inp, g, seed, distance_type);
        
        betti_curves = [];
        betti_curves = utility.sampledBettiCurves(name, _d, max_threshold);
        
        #calculate corresponding threshold
        threshold = np.linspace(0, max_threshold, (int(max_threshold/_d) + 1)); #same size as the betti curve
        
        #dim = 0
        betti_0 = betti_curves[0];
        
        #max
        betti_0_max = max(betti_0);
        measures['{}_beta_0_max'.format(distance_type)] = betti_0_max;
        
        # halfmax threshold
        betti_0_halfmax = betti_0_max/2;
        threshold_halfmax = 1; #max by default, reduced if one found before
        
        size = len(betti_0);
        for i in range(0, size):
            if betti_0[i] <= betti_0_halfmax:
                #first threshold found, betti_0 is monotone
                threshold_halfmax = threshold[i];
                break;
        
        measures['{}_beta_0_halfmax_threshold'.format(distance_type)] = threshold_halfmax;
        
        # area under curve
        measures['{}_beta_0_auc'.format(distance_type)] = simps(betti_0, threshold); #using Simpson's integration from SciPy
        
        #dim = 1
        betti_1 = betti_curves[1];
        
        # max
        betti_1_max = max(betti_1);
        measures['{}_beta_1_max'.format(distance_type)] = betti_1_max;
        
        # max threshold
        betti_1_max_threshold_index = np.argmax(betti_1);
        measures['{}_beta_1_max_threshold'.format(distance_type)] = threshold[betti_1_max_threshold_index];
        
        # area under curve
        measures['{}_beta_1_auc'.format(distance_type)] = simps(betti_1, threshold); #using Simpson's integration from SciPy
        
    return measures;
    
def ComputeConnectivityMeasure(connect_model):
    for cseedling in range(0,1): #replication
        #connectivity measures
        connectivity_measures = {};
        connectivity_measures.update(ConnectivityMeasure(connect_model, cseedling));
        
        #save connectivity measures
        utility.saveData(connectivity_measures, 'data/measures/connectivity_measures_{}_{}.csv'.format(connect_model, cseedling))

def ComputeFunctionalMeasure(g, inp, connect_model):
    for cseedling in range(0,1): #replication
        for seedling in range(0,parameters._replication):
            #functional measures
            functional_measures = {};
            #functional_measures.update(FunctionalMeasure(g, inp, connect_model, seedling, cseedling)); #too long, would take at least a week
            functional_measures.update(AlgebraicTopologyMeasure(g, inp, connect_model, seedling, cseedling));
            
            #save functional measures
            utility.saveData(functional_measures, 'data/measures/functional_measures_{}_{}_i={}_g={}_s={}.csv'.format(connect_model, cseedling, inp, g, seedling))

#try:
            
for cm in connect_models:
    print(cm);
    #ComputeConnectivityMeasure(cm);
    for g in [2,4,6]:
        print('g={}'.format(g));
        for inp in [1,2,3,4]:
            print('inp={}'.format(inp));
            ComputeFunctionalMeasure(g, inp, cm);
        
#except Exception as e:
#    print('Error:')
#    print(str(e))